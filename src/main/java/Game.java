import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Game {

	private int[][] 			frames;
	private int 				frameNumber;
	private int 				frameShoot;
	private int 				score;

	public Game() 
	{
		frames = new int[12][3];
		frameNumber = 0;
		frameShoot = 0;
		score = 0;
	}

	/**
	 * Rememeber pins of the shoot.
	 * 
	 * @param pins
	 */
	public void roll(int pins) {
		if (pins == 10 && frameShoot == 0) {
			frames[frameNumber][frameShoot] = pins;

			frameNumber += 1;
			frameShoot = 0;

		} 
		else if(pins == 10 && frameShoot == 0 && frameNumber == 11)
		{
			frames[frameNumber][frameShoot] = pins;

			frameShoot += 1;
		}
		
		else {
			frames[frameNumber][frameShoot] = pins;
			frameShoot += 1;

			if (frameShoot > 1) {
				frameShoot = 0;
				frameNumber += 1;
			}

		}

	}

	/**
	 * Return total score.
	 * 
	 * @return
	 */
	public int score() {

		score = 0;
		
		for (int i = 0; i <= 10; i++) {
			// Caso strike
			if (frames[i][0] == 10) {
				score += 10 + frames[i + 1][0] + frames[i + 1][1] + frames[i + 1][2];
			}
			// Caso spare
			else if (frames[i][0] + frames[i][1] == 10) {
				score += 10 + frames[i + 1][0];
			} else {
				score += frames[i][0] + frames[i][1];
			}

		}

		Logger logger = LoggerFactory.getLogger(Game.class);
	    logger.info("Punteggio: " + Integer.toString(score));

	    frames = new int[12][2];
		frameNumber = 0;
		frameShoot = 0;
		
		return score;

	}

}
